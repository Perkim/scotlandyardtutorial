package com.projektesch.denwe.tictacthree;

import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    Context context;
    PlayerInteraction createGame;
    PlayerInteraction joinGame;
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createGame = new PlayerInteraction();
        createGame.setPlayerAction(PlayerAction.CREATE_GAME);

        joinGame = new PlayerInteraction();
        joinGame.setPlayerAction(PlayerAction.JOIN_GAME);

        context = this.getApplicationContext();


        text = (EditText) findViewById(R.id.editText2);


        Button buttonOne = (Button) findViewById(R.id.button);
        buttonOne.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NetworkTask().execute(new Pair<Context, PlayerInteraction>(context, createGame));
            }
        });

        Button buttonTwo = (Button) findViewById(R.id.button4);
        buttonTwo.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinGame.setId(Integer.valueOf(text.getText().toString()));
                new NetworkTask().execute(new Pair<Context, PlayerInteraction>(context, joinGame));
            }
        });
    }
}
