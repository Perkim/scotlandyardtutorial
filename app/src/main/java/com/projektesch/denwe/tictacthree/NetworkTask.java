package com.projektesch.denwe.tictacthree;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Debug;
import android.support.v4.util.Pair;
import android.util.Log;
import android.widget.Toast;

import com.example.denwe.myapplication.backend.myApi.MyApi;
import com.example.denwe.myapplication.backend.myApi.model.GameState;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;

import java.io.IOException;

/**
 * Created by denwe on 13.06.2017.
 */

public class NetworkTask  extends AsyncTask<Pair<Context, PlayerInteraction>, Void, GameState> {

    PlayerAction pa;
    Context context;
    private static MyApi myApiService = null;

    @Override
    protected GameState doInBackground(Pair<Context, PlayerInteraction>... params) {



        if(myApiService == null) {  // Only do this once
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    // options for running against local devappserver
                    // - 10.0.2.2 is localhost's IP address in Android emulator
                    // - turn off compression when running against local devappserver
                    .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            // end options for devappserver

            myApiService = builder.build();
        }

        PlayerInteraction pi = params[0].second;
        context = params[0].first;

        Log.v("[NETWORKTASK]", "1");

        try {
            switch(pa = pi.getPlayerAction()) {
                case JOIN_GAME:
                    return myApiService.joinGame(pi.getId()).execute();
                case CREATE_GAME:
                    return myApiService.createGame(true).execute();
                case SUBMIT_MOVE:
                    return myApiService.makeMove(pi.getId(),pi.getX(),pi.getY(),pi.getPlayerId()).execute();
                default:
                    return null;
            }

        } catch (IOException e) {
            return null;
        }


    }

    @Override
    protected void onPostExecute(GameState result) {

        if(result != null) {


            switch (pa) {

                case JOIN_GAME:


                    Toast.makeText(context, "Join gut :" +result.getJoinSuccessful()  , Toast.LENGTH_LONG).show();

                    break;
                case CREATE_GAME:

                    if (result.getGame() != null) {
                        Toast.makeText(context, "Game Creation gut :" +result.getGame().getId()  , Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(context, "Game Creation failed", Toast.LENGTH_LONG).show();
                    }
                    break;
                case SUBMIT_MOVE:
                    break;
                default:
                    Toast.makeText(context, "DEFAULT", Toast.LENGTH_LONG).show();
                    break;

            }
        } else {

            Log.v("[NETWORKTASK]", "NULL RETURNED");
        }

        Log.v("[NETWORKTASK]", pa.toString());
    }
}
