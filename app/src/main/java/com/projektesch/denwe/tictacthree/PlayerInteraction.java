package com.projektesch.denwe.tictacthree;

/**
 * Created by denwe on 13.06.2017.
 */

public class PlayerInteraction {

    PlayerAction playerAction;

    private int x,y;
    private int id;
    private int playerId;

    private int gameId;

    public PlayerAction getPlayerAction() {
        return playerAction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getId() {
        return id;
    }

    public void setPlayerAction(PlayerAction playerAction) {
        this.playerAction = playerAction;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
