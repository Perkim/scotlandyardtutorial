package com.example.denwe.myapplication.backend;

import com.google.appengine.repackaged.org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created by denwe on 13.06.2017.
 */

public class Game {

    final int playerOne = 1;
    final int playerTwo = 2;

    private int id;

    private int board[][] = {   {0,0,0},
            {0,0,0},
            {0,0,0} };
    boolean turn;
    private int currentPlayer = 1;
    private boolean gameOver = false;
    private int winner = 0;
    private boolean isStarting = false;

    public Game() {

    }

    public Game(int id) {
        this.id = id;
    }

    @JsonIgnore
    private boolean isValid(int x, int y) {
        if(board[x][y] == 0) return true;
        return false;
    }
    @JsonIgnore
    private boolean placeTile(int x, int y, int player) {
        if(isValid(x,y)) {
            if (player == playerOne) {
                board[x][y] = playerOne;
                return true;
            } else {
                board[x][y] = playerTwo;
                return true;
            }
        }
        return false;
    }
    @JsonIgnore
    private boolean checkForWinHorizontal() {
        for(int i = 0; i < 3;i++) {
            if((board[i][0] == board[i][1]) && (board[i][2] == board[i][1])) {
                return true;
            }
        }
        return false;
    }
    @JsonIgnore
    private boolean checkForWinVerticals() {
        for(int i = 0; i < 3;i++) {
            if((board[0][i] == board[1][i]) && (board[2][i] == board[1][i])) {
                return true;
            }
        }
        return false;
    }
    @JsonIgnore
    private boolean checkForWinDiagonals() {
        if((board[0][0] == board[1][1]) && (board[2][2] == board[1][1])) {
            return true;
        }

        if((board[0][2] == board[1][1]) && (board[3][0] == board[1][1])) {
            return true;
        }
        return false;
    }
    @JsonIgnore
    public boolean makeMove(int x, int y, int player) {
        if(placeTile(x,y,player)) {
            currentPlayer = player;
            if(checkForWin()) {
                gameOver = true;
                winner = currentPlayer;
            }
        }

        return false;

    }
    @JsonIgnore
    public boolean checkForWin() {
        return checkForWinDiagonals() || checkForWinHorizontal() || checkForWinVerticals();
    }

    public void setIsStarting(boolean start) {
        this.isStarting = start;
    }

    public GameBean getBean() {

        GameBean bean = new GameBean();

        bean.setBoard(board);
        bean.setCurrentPlayer(currentPlayer);
        bean.setGameOver(gameOver);
        bean.setId(id);
        bean.setStarting(isStarting);
        bean.setTurn(turn);
        bean.setWinner(winner);
        return bean;

    }
}
