/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.example.denwe.myapplication.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import java.util.HashMap;

import javax.inject.Named;

/**
 * An endpoint class we are exposing
 */
@Api(
        name = "myApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "backend.myapplication.denwe.example.com",
                ownerName = "backend.myapplication.denwe.example.com",
                packagePath = ""
        )
)
public class MyEndpoint {


    HashMap<Integer,Game> gameList = new HashMap<Integer, Game>();
    int id = 0;

    /**
     * A simple endpoint method that takes a name and says Hi back
     */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi(@Named("name") String name) {
        MyBean response = new MyBean();
        response.setData("Hi, " + name);

        return response;
    }

    @ApiMethod(name = "createGame")
    public GameState createGame(@Named("createGame") boolean request) {
        GameState response = new GameState();

        Game game = new Game(id);
        gameList.put(id, game);
        response.setData(true,game);
        id++;

        return response;
    }

    @ApiMethod(name = "joinGame")
    public GameState joinGame(@Named("gameId") int id) {
        GameState response = new GameState();

        Game game;
        if((game = gameList.get(id)) != null) {
            response.setJoinSuccessful(true);
            game.setIsStarting(true);
        }
        else response.setJoinSuccessful(false);
        return response;
    }

    @ApiMethod(name = "makeMove")
    public GameState makeMove(@Named("gameId") int id, @Named("posX") int x, @Named("posY") int y, @Named("player") int player) {

        GameState response = new GameState();
        Game game;


        if((game = gameList.get(id)) != null) {
            if(game.makeMove(x,y, player)) {
                response.setData(true, game);
            } else {
                response.setData(false,game);
            }
        }
        else response.setData(false,null);
        return response;

    }


}
