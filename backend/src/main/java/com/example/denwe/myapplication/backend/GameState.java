package com.example.denwe.myapplication.backend;

/**
 * Created by denwe on 13.06.2017.
 */

public class GameState {

    private boolean isGameCreated;
    private boolean isMoveSuccessful;
    private boolean isJoinSuccessful;

    private GameBean gameBean;

    public void setGameCreated(boolean isGameCreated) {this.isGameCreated = isGameCreated;}


    public void setJoinSuccessful(boolean isJoinSuccessful) {this.isJoinSuccessful = isJoinSuccessful;}
    public boolean getJoinSuccessful(){ return isJoinSuccessful;}

    public void setData(boolean isMoveSuccessful, Game game) {
        this.isMoveSuccessful = isMoveSuccessful;
        this.gameBean = game.getBean();
    }

    public boolean wasMoveSuccessful() {
        return isMoveSuccessful;
    }

    public GameBean getGame() {
        return gameBean;
    }
}
